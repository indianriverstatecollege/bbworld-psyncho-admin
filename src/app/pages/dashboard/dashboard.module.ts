import { NgModule } from '@angular/core';

import { ThemeModule } from '../../@theme/theme.module';
import { DashboardComponent } from './dashboard.component';
import { MonitorPanelComponent } from './monitor-panel/monitor-panel.component';
import { StreamViewerComponent } from './monitor-panel/stream-viewer/stream-viewer.component';
import { DataItemComponent } from './monitor-panel/stream-viewer/data-item/data-item.component';

@NgModule({
  imports: [
    ThemeModule,
  ],
  declarations: [
    DashboardComponent,
    DataItemComponent,
    MonitorPanelComponent,
    StreamViewerComponent,
  ],
  providers: [],
})
export class DashboardModule { }
