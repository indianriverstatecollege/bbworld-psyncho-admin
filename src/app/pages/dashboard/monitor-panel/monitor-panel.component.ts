import { Component, ViewChild } from '@angular/core';
import { DataService } from '../../../@core/utils';

import { StreamViewerComponent } from './stream-viewer/stream-viewer.component';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'ngx-monitor-panel',
  styleUrls: ['./monitor-panel.component.scss'],
  templateUrl: './monitor-panel.component.html',
})
export class MonitorPanelComponent {

  public response = [];
  public gqlExt: any;
  public endpoint: string;
  @ViewChild('streamViewer') streamViewer: StreamViewerComponent;

  constructor(private dataService: DataService, private sanitizer: DomSanitizer) {
    this.endpoint = this.dataService.endpoint;
    this.gqlExt = this.sanitizer.bypassSecurityTrustResourceUrl(this.endpoint);
  }

}
