import { Component, Input, Output } from '@angular/core';
import { DataService } from '../../../../../@core/services/data.service';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'ngx-data-item',
  styleUrls: ['./data-item.component.scss'],
  templateUrl: 'data-item.component.html',
})
export class DataItemComponent {

  hideBtn = [];
  dataItems = this.dataService.allData;
  @Input() filterData = new BehaviorSubject('');
  @Input() itemIndex = 0;
  @Input() item: any;
  @Output() prettyJSON: string;
  @Output() highlightJSON: string;

  constructor(private dataService: DataService) {
    this.filterData.next(this.dataService.inputSubject.getValue());
  }

  hideEvent(index: number) {
    this.prettyJSON = '';
    this.hideBtn[index] = false;
    this.dataService.pause.next(false);
  }

  displayJsonHtml(data: any) {
    const str = (typeof data === 'string') ? data : JSON.stringify(data, undefined, 4);
    this.highlightJSON = this.dataService.highlightJSON(str, this.dataService.inputSubject.getValue(), 0);
    return this.highlightJSON;
  }

  showEvent(data: any, index: number) {
    const str = JSON.stringify(data, undefined, 4);
    this.prettyJSON = this.dataService.syntaxJSON(str, this.dataService.inputSubject.getValue());
    this.hideBtn[index] = true;
    this.dataService.pause.next(true);
  }

}
