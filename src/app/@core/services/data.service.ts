import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import * as socketIo from 'socket.io-client';

import { Socket } from '../utils/interfaces';
import { Subject } from 'rxjs/Subject';
import { BehaviorSubject } from 'rxjs';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';

/**
 * Toaster Imports
 */
import { ToasterConfig } from 'angular2-toaster';
import 'style-loader!angular2-toaster/toaster.css';
import { NbGlobalLogicalPosition, NbGlobalPhysicalPosition, NbGlobalPosition, NbToastrService } from '@nebular/theme';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
/** END Toaster Imports */


// declare var io: {
//   connect(url: string): Socket;
// };

@Injectable({
  providedIn: 'root',
})
export class DataService {

  private socket: Socket;
  private observer: Observer<any>;
  public alive = new BehaviorSubject(false);
  public pause = new BehaviorSubject(false);
  public allData = [];
  inputSubject = new BehaviorSubject('');

  //
  private notify = new Subject<any>();
  /**
   * Observable string streams
   */
  notifyObservable$ = this.notify.asObservable();

  public notifyOther(data: any) {
    if (data) {
      this.notify.next(data);
    }
  }
  //

  public endpoint: string = 'MISSING ENDPOINT - Please configure the GraphQL Endpoint in the environment config';

  constructor(private toastrService: NbToastrService, private http: HttpClient) {

    this.endpoint = environment.graphql;
    this.socket = socketIo(environment.socket);
    this.pause.next(false);
    // UNCOMMENT TO SEND MESSAGES...
    // interval(3000).subscribe((i: any) => {
    //   this.send({message: 'HELLOOOOOO!', data: -1});
    //   // this.send({test: 'TESTING!!!', data: -1});
    // });

  }

  /**
   * Toaster Configuration
   */
  config: ToasterConfig;

  index = 1;
  destroyByClick = true;
  duration = 2000;
  hasIcon = true;
  position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
  preventDuplicates = false;
  status: NbToastStatus = NbToastStatus.SUCCESS;

  title = '';
  content = ``;

  types: NbToastStatus[] = [
    NbToastStatus.DEFAULT,
    NbToastStatus.DANGER,
    NbToastStatus.INFO,
    NbToastStatus.PRIMARY,
    NbToastStatus.SUCCESS,
    NbToastStatus.WARNING,
  ];
  positions: string[] = [
    NbGlobalPhysicalPosition.TOP_RIGHT,
    NbGlobalPhysicalPosition.TOP_LEFT,
    NbGlobalPhysicalPosition.BOTTOM_LEFT,
    NbGlobalPhysicalPosition.BOTTOM_RIGHT,
    NbGlobalLogicalPosition.TOP_END,
    NbGlobalLogicalPosition.TOP_START,
    NbGlobalLogicalPosition.BOTTOM_END,
    NbGlobalLogicalPosition.BOTTOM_START,
  ];

  makeToast() {
    this.showToast(this.status, this.title, this.content);
  }

  private showToast(type: NbToastStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: this.destroyByClick,
      duration: this.duration,
      hasIcon: this.hasIcon,
      position: this.position,
      preventDuplicates: this.preventDuplicates,
    };
    const titleContent = title ? `${title}` : '';

    this.index += 1;
    this.toastrService.show(
      body,
      `${titleContent}`,
      config);
  }
  /** END Toaster Code */

  //
  send(message: any): void {
    this.socket.emit('message', message);
  }

  onMessage(): Observable<any> {
    return new Observable<any>(observer => {
        this.socket.on('message', (data: any) => {
          observer.next(data);
          this.status = NbToastStatus.WARNING;
          this.title = 'Errors';
          this.content = data.message;
          this.showToast(this.status, this.title, this.content);
        });
    });
  }

  onEvent(event: any): Observable<any> {
    return new Observable<any>(observer => {
        this.socket.on(event, () => observer.next());
    });
  }
  //

  getTimeStamp() {
    return `${new Date().toISOString()}`;
  }

  getEventType(item: any) {
    let eventType = 'Unknown';

    if (item) {
      if (item.hasOwnProperty('error')) {
        eventType = 'Errors';
      }
      if (item.hasOwnProperty('courses')) {
        eventType = 'Courses';
      }
      if (item.hasOwnProperty('collections')) {
        eventType = 'Collections';
      }
      if (item.hasOwnProperty('enrollments')) {
        eventType = 'Enrollments';
      }
      if (item.hasOwnProperty('roles')) {
        eventType = 'Roles';
      }
      if (item.hasOwnProperty('terms')) {
        eventType = 'Terms';
      }
      if (item.hasOwnProperty('_id')) {
        eventType = 'Database Updates';
      }
      if (item.hasOwnProperty('users')) {
        eventType = 'Users';
      }
    }

    return eventType;
  }

  onSocket(sock: Socket, watch: string) {
    sock.on(watch, (data: any) => {
      if (data) {
        data.timeStamp = this.getTimeStamp();
        data.eventType = this.getEventType(data);
        this.observer.next(data);
      } else {
        this.observer.next({ 'error': `No data was returned on socket: ${watch}`, 'eventType': 'Errors'});
      }
    });
  }

  getData(): Observable<any> {

    this.onSocket(this.socket, 'active:courses');
    this.onSocket(this.socket, 'database:collections');
    this.onSocket(this.socket, 'active:courses:updates');

    return this.createObservable();
  }

  createObservable(): Observable<any> {
      return new Observable<any>(observer => {
        this.observer = observer;
      });
  }

  private handleError(error: any) {
    console.error('server error:', error);
    if (error.error instanceof Error) {
        const errMessage = error.error.message;
        return Observable.throw(errMessage);
    }
    return Observable.throw(error || 'Socket.io server error');
  }

  highlightJSON(str: string, termsToHighlight: any, limit: any) {

    // substring but show the match
    if (limit) {
      let iStart = str.indexOf(termsToHighlight) - 30;
      if (iStart < 0)
        iStart = 0;
      str = str.substring(iStart, iStart + limit);
    }

    if (typeof termsToHighlight === 'undefined' || termsToHighlight === '') {
      return str;
    }

    // Regex to simultaneously replace terms
    const regex = new RegExp('(' + termsToHighlight + ')', 'ig');
    return str.replace(regex, '<span class="highlight">$&</span>');
  }

  syntaxJSON(json: any, highlight: string) {
    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    // tslint:disable-next-line: max-line-length
    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, (match: string) => {
        let cls = 'number';
        if (/^"/.test(match)) {
            if (/:$/.test(match)) {
                cls = 'key';
            } else {
                cls = 'string';
            }
        } else if (/true|false/.test(match)) {
            cls = 'boolean';
        } else if (/null/.test(match)) {
            cls = 'null';
        }
        match = this.highlightJSON(match, highlight, 0);
        return '<span class="' + cls + '">' + match + '</span>';
    });
  }

  async sendPayload(payload: any, vars: any) {
    console.log(`PAYLOAD = ${payload} \nVARIABLES = ${vars}`);

    const httpOptions = {
      headers: new HttpHeaders({
        'accept': 'application/json',
        'Content-Type':  'application/json',
      }),
    };

    this.http.post('/psynchoql', JSON.stringify({ query: payload, variables: vars }), httpOptions)
    .subscribe(console.log);
    //     .then(resp => {
    //       if (resp.status === 200) {
    //           return resp.json();
    //       } else {
    //           console.log('Status: ' + resp.status);
    //           return Promise.reject('server');
    //       }
    //     })
    //     .then(dataJson =>{
    //           dataReceived = JSON.parse(dataJson);
    //           console.log(dataReceived);
    //       })
    //       .catch(err => {
    //         if (err === 'server') return;
    //         console.log(err);
    //     });
  }

}
