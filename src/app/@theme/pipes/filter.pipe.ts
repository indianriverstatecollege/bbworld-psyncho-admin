import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'filterPipe' })
export class FilterPipe implements PipeTransform {

  transform(items: any[], searchText: string): any[] {
    if (!items) return [];
    if (!searchText) return items;
    const tempItems = items.filter( (it: any) => {
      const re = new RegExp(searchText, 'gim');
      const itemStr = JSON.stringify(it);
      const isMatch = re.test(itemStr);
      return isMatch;
    });
    return tempItems;
   }
}
