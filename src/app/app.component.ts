import { Component, OnInit, OnDestroy } from '@angular/core';
import { AnalyticsService } from './@core/services/analytics.service';
import { DataService } from './@core/services/data.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'ngx-app',
  template: '<router-outlet></router-outlet>',
})
export class AppComponent implements OnInit, OnDestroy {

  sub: Subscription;
  sub2: Subscription;
  messages: any[] = [];

  constructor(private analytics: AnalyticsService,
    private dataService: DataService) {
  }

  ngOnInit(): void {
    // this.analytics.trackPageViews();
    // this.dataService.pause = false;

    this.sub = this.dataService.getData()
      .subscribe(data => {
        this.dataService.notifyOther({data});
      });

    //
    this.sub2 = this.dataService.onMessage()
      .subscribe((message: any) => {
        this.messages.push(message);
        // this.dataService.content = message.message;
        // this.dataService.makeToast();
        console.log(message);
      });

    this.dataService.onEvent('connect')
      .subscribe(() => {
        this.dataService.alive.next(true);
        console.log('connected');
      });

    this.dataService.onEvent('disconnect')
      .subscribe(() => {
        this.dataService.alive.next(false);
        console.log('disconnected');
      });
    //
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
    this.sub2.unsubscribe();
  }
}
