import { Component, OnDestroy, OnInit } from '@angular/core';
import { DataService } from '../../../../@core/utils';
import { BehaviorSubject, combineLatest, Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'ngx-stream-viewer',
  styleUrls: ['./stream-viewer.component.scss'],
  templateUrl: './stream-viewer.component.html',
})
export class StreamViewerComponent implements OnDestroy, OnInit {

  public all = [];
  public courses = [];
  public enrollments = [];
  public roles = [];
  public terms = [];
  public updates = [];
  public users = [];
  public errors = [];
  public unsubscribe = new Subject();
  public alive: BehaviorSubject<boolean>;
  public pause: BehaviorSubject<boolean>;
  toggleBool = false;

  results = new BehaviorSubject([]);
  inputSubject = new BehaviorSubject('');

  dataCard = {
    data: [],
    loading: false,
  };
  pageSize = 1;

  constructor(
    private dataService: DataService,
  ) {}

  filterResults(value: string) {
    if (value.length >= 3) {
      this.inputSubject.next(value);
      this.dataService.inputSubject.next(value);
    } else {
      this.inputSubject.next('');
      this.dataService.inputSubject.next('');
    }
  }

  togglePause() {
    this.toggleBool = !this.toggleBool;
    this.dataService.pause.next(this.toggleBool);
    this.pause.next(this.toggleBool);
  }

  checkEmpty(obj: any) {
    return (obj.length > 0) ? true : false;
  }

  ngOnInit() {
    this.alive = this.dataService.alive;
    this.pause = this.dataService.pause;

    combineLatest(
      this.dataService.notifyObservable$,
    )
    .pipe(
      takeUntil(this.unsubscribe),
      map(([data]: any) => {
        return data;
      }),
    )
    .subscribe((res) => {
      if (!this.pause.getValue()) {
        let str: string;
        str = JSON.stringify(res.data, undefined, 4);

        this.all.unshift(res);
        this.dataService.allData = this.all;
        this.dataCard.data = this.dataService.allData;

        if (res.data) {
          if (res.data.hasOwnProperty('courses')) {
            this.courses = this.dataService.syntaxJSON(str, this.dataService.inputSubject.getValue());
          }
          if (res.data.hasOwnProperty('enrollments')) {
            this.enrollments = this.dataService.syntaxJSON(str, this.dataService.inputSubject.getValue());
          }
          if (res.data.hasOwnProperty('roles')) {
            this.roles = this.dataService.syntaxJSON(str, this.dataService.inputSubject.getValue());
          }
          if (res.data.hasOwnProperty('terms')) {
            this.terms = this.dataService.syntaxJSON(str, this.dataService.inputSubject.getValue());
          }
          if (res.data.hasOwnProperty('_id')) {
            this.updates = this.dataService.syntaxJSON(str, this.dataService.inputSubject.getValue());
          }
          if (res.data.hasOwnProperty('users')) {
            this.users = this.dataService.syntaxJSON(str, this.dataService.inputSubject.getValue());
          }
          if (res.data.hasOwnProperty('error')) {
            this.errors = this.dataService.syntaxJSON(str, this.dataService.inputSubject.getValue());
          }
        }
      }

    });
  }

  ngOnDestroy() {
    this.unsubscribe.next();
  }

}
