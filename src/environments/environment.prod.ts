export const environment = {
  production: true,
  socket: 'http://localhost:8080',
  graphql: 'http://localhost:4000/psynchoql/',
};
